# runescape**d** #

runescaped is a RuneScape server daemon forked from [Project Insanity](https://www.rune-server.org/runescape-development/rs2-server/downloads/227272-project-insanity.html).

## Getting Started ##

To run runescaped on your server, you will need only two main components:

* runescaped, the RuneScape server daemon
* Java Runtime Environment & Java Development Kit, version(s) 8 or later. OpenJDK is recommended, but compatibility is maintained with Oracle too.

In addition to the main components, you will also need:

* `git`, version 1.8.5 or later (tip: if the `-C` option is not recognized, your version is too old)

### Step 1: **Cloning** ###

Requisite: a functional installation of `git`.

Simply clone this repository. We will use `/opt` as an example installation location, but realistically you can use anywhere you like.

```
cd /opt
git clone https://bitbucket.org/KeepBotting/runescaped
cd runescaped
```

### Step 2: **Compiling** ###

Requisite: a functional installation of Oracle's Java Development Kit.

Now we will use `javac` to compile runescaped.


```
mkdir bin
javac -cp "deps/*" -d bin/ $(find . -iname *.java)

```

If all goes well, you should receive output similar to the below:

```
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
```

This message is perfectly normal and expected, in fact it signals that compilation went off without a hitch, and you're ready to move on. If, on the other hand, you receive errors, you'll need to resolve them before continuing.

### Step 3: **Executing** ###

Requisite: a functional installation of Oracle's Java Runtime Environment.


Once runescaped is compiled, it is ready to run out of the box.

```
export RSD_PATH=/opt/runescaped
java -Xmx1024m -cp $RSD_PATH/bin:"$RSD_PATH/deps/*" server.Server
```

The value of `RSD_PATH` should reflect the actual install location, in case you did not install runescaped to `/opt`.

The `-Xmx1024m` parameter can be altered to allocate more (or less) of your server's memory to runescaped, e.g. `-Xmx512m` for 512 MB or `-Xmx2048m` for 2 GB.

## Updating ##

Updating can be divided into three simple steps: pulling, recompiling, and restarting.

### Step 1: **Pulling** ###

Again, here we are using `/opt` as an example installation location. Adjust accordingly if you have installed runescaped somewhere else.

```
git -C /opt/runescaped pull
```

### Step 2: **Recompiling** ###

This step is almost identical to **Step 2: Compiling** under the **Getting Started** header. Refer to it (above) for a more in-depth description.

```
cd /opt/runescaped
javac -cp "deps/*" -d bin/ $(find . -iname *.java)
```

### Step 3: **Restarting** ###

The intricacies of this step will differ depending on your setup. If you have chosen to daemonize runescaped, you should leverage your init system to restart the daemon. If you are running runescaped directly from the command line, or in another way that is non-daemonized, you should kill the process using some variant of `kill` and then restart it using the commands detailed in **Step 3: Executing** under the **Getting Started** header.

Bottom line: for the updates to take effect, the server must be restarted. Accomplish this using whatever means you choose.

## Daemonizing ##

runescaped works best when managed by an init system. Here are instructions for getting runescaped daemonized properly under a few different init systems.

Where possible, runescaped will be run as the unpriveleged user `runescaped`. Please ensure that this user exists and has access to the appropriate files and directories.

 As always, `/opt` is used as an example in all the following sections.

### System V ###

A System V init script is located in `init/init.sysv`. It has been tested on Debian 8.

```
# todo
```

### Upstart ###

An Upstart script is located in `init/init.upstart`. It has been tested on Ubuntu 14.04.

Copy the Upstart script to `/etc/init` and create the required symlink to `upstart-job`:

```
sudo cp /opt/runescaped/init/init.upstart /etc/init/runescaped.conf
sudo ln -s /lib/init/upstart-job /etc/init.d/runescaped
```

Ensure that the Upstart script knows where runescaped is located:

```
echo "/opt/runescaped" | sudo tee /etc/default/runescaped
```

At this point, `sudo service runescaped (start|stop|restart|status)` becomes available.

### systemd ###

A systemd service file is located in `init/init.systemd`. It has been tested on Debian 8.

```
# todo
```

### runit ###

A runit script is located in `init/init.runit`. It has been tested on Void Linux.

```
# todo
```

### OpenRC ###

An OpenRC script is located in `init/init.openrc`. It has not been tested.

```
# todo
```
